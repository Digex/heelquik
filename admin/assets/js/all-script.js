(function($){
  $(document).ready(function() {

    $('#fecha_nacimiento').datepicker({
    	changeMonth: true,
    	changeYear: true,
    	yearRange: '1950:2020',
    	dateFormat: 'yy-mm-dd'
    });
        
  });
})(jQuery);

