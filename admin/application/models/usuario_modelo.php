<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuario_modelo extends CI_Model 
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function validar_usuario($user, $pass)
	{
		$sql = "SELECT id_usuario, nombre, email, tipo
				FROM usuarios
				WHERE usuario = ?
				AND password = ?
				AND status = 0 LIMIT 1;";
		
		$query = $this->db->query($sql, array($user, md5($pass)));
		
		return $query->row();
	}
	
	public function validar_usuario_super($user, $pass)
	{
		$sql = "SELECT id_usuario, nombre, email, tipo
				FROM usuarios
				WHERE usuario = ?
				AND password = ?
				AND status = 0 AND tipo = 1 LIMIT 1;";
		
		$query = $this->db->query($sql, array($user, md5($pass)));
		
		return $query->row();
	}
	
	public function obtener_usuario($id)
	{
		$sql = "SELECT id_usuario, nombre, email
				FROM usuarios WHERE id_usuario = ?;";
		
		$query = $this->db->query($sql, array($id));
		
		return $query->row();
	}
	
	/* Gestor */
	public function obtener_usuarios($where = 'ORDER BY u.id_usuario ASC')
	{
		$sql = "SELECT u.id_usuario, u.nombre, u.email, u.tipo,
				u.usuario, u.fecha_registro, u.fecha_modificacion, u.status
				FROM usuarios AS u
				".$where.";";
		
		$query = $this->db->query($sql);
		
		return $query->result();
	}
	
	public function obtener_usuario_detalle($id)
	{
		$sql = "SELECT u.id_usuario, u.nombre, u.email, u.tipo,
				u.usuario, u.fecha_registro, u.fecha_modificacion, u.status
				FROM usuarios AS u
				WHERE u.id_usuario = ?;";
		
		$query = $this->db->query($sql, array($id));
		
		return $query->row();
	}
	
	public function insertar_usuario($datos)
	{
		$this->db->insert('usuarios', $datos);
		return $this->db->insert_id();
	}
	
	public function actualizar_usuario($id, $datos)
	{
		$this->db->where('id_usuario', $id);
		$this->db->update('usuarios', $datos);
		return $this->db->affected_rows();
	}
	
	public function existe_usuario($usuario)
	{
		$sql = "SELECT id_usuario
				FROM usuarios
				WHERE usuario = ?
				LIMIT 1;";
		
		$query = $this->db->query($sql, array($usuario));
		
		return $query->row();
	}

	public function eliminar_usuario($id)
	{
		
		$this->db->where('id_usuario', $id);
		return $this->db->delete('usuarios'); 
	}
}