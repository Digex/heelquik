<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Seguridad_modelo extends CI_Model
{
	var $salt = 'BSd9a-#$8mxe3dk--fde353sd0a+9d8a0gv0*';
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function sellar_sesion($auth)
	{
		return md5($this->salt.$auth['id_usuario'].$auth['nombre'].$auth['email']);
	}
	
	public function validar_sesion()
	{
		$auth = $this->session->userdata("auth");
		
		if( md5($this->salt.$auth['id_usuario'].$auth['nombre'].$auth['email']) == $auth['sello'] )
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function generar_sello($valor)
	{
		return md5($this->salt.$valor);
	}
	
	public function verificar_sello($valor, $sello)
	{
		if( md5($this->salt.$valor) == $sello )
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function generar_token()
	{
		$aleatorio = rand();
		
		$token     = md5($this->salt.$aleatorio);
		
		$this->session->set_userdata("token", $token);
	}
	
	public function verificar_token($token)
	{
		$token_servidor = $this->session->userdata("token");
		
		$token_cliente  = $token;
		
		if(strlen($token_servidor) < 30) return false;
		
		if ($token_servidor != $token_cliente) return false;
		
		return true;
		
	}
}