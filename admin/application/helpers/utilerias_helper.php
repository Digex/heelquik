<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	function escribir_codigo($codigo)
	{
		$part1 = substr($codigo, 0, 4);
		$part2 = substr($codigo, 4, 4);
		$part3 = substr($codigo, 8, 4);
		$part4 = substr($codigo, 12, 4);
		
		return $part1.'-'.$part2.'-'.$part3.'-'.$part4;
	}
	
	function fecha_formato($fecha, $opcion = 0, $separador = "-")
	{
		if ($opcion == 0) { return fecha_organizada_mes_completo($fecha, $separador);}
		
		if ($opcion == 1) { return fecha_organizada_mes_truncado($fecha, $separador);}
		
		if ($opcion == 3) { return fecha_organizada_mes_completo_hora($fecha, $separador);}
		
		if ($opcion == 4) { return fecha_organizada_mes_truncado_hora($fecha, $separador);}
		
	}
	
	function fecha_organizada_mes_completo($fecha, $separador)
	{
		$arreglo_fecha = explode(" ", $fecha);
		$arreglo_fecha = explode("-", $arreglo_fecha[0]);
		
		$mes  = "";
		$dia  = $arreglo_fecha[0];
		$anio = $arreglo_fecha[2];
		
		if ($arreglo_fecha[1] == "01") $mes = "Enero";
		if ($arreglo_fecha[1] == "02") $mes = "Febrero";
		if ($arreglo_fecha[1] == "03") $mes = "Marzo";
		if ($arreglo_fecha[1] == "04") $mes = "Abril";
		if ($arreglo_fecha[1] == "05") $mes = "Mayo";
		if ($arreglo_fecha[1] == "06") $mes = "Junio";
		if ($arreglo_fecha[1] == "07") $mes = "Julio";
		if ($arreglo_fecha[1] == "08") $mes = "Agosto";
		if ($arreglo_fecha[1] == "09") $mes = "Septiembre";
		if ($arreglo_fecha[1] == "10") $mes = "Octubre";
		if ($arreglo_fecha[1] == "11") $mes = "Noviembre";
		if ($arreglo_fecha[1] == "12") $mes = "Diciembre";
		
		return $anio.$separador.$mes.$separador.$dia;
	}
	
	function fecha_organizada_mes_truncado($fecha, $separador)
	{
		$arreglo_fecha = explode(" ", $fecha);
		$arreglo_fecha = explode("-", $arreglo_fecha[0]);
		
		$mes  = "";
		$dia  = $arreglo_fecha[0];
		$anio = $arreglo_fecha[2];
		
		if ($arreglo_fecha[1] == "01") $mes = "01";
		if ($arreglo_fecha[1] == "02") $mes = "02";
		if ($arreglo_fecha[1] == "03") $mes = "03";
		if ($arreglo_fecha[1] == "04") $mes = "04";
		if ($arreglo_fecha[1] == "05") $mes = "05";
		if ($arreglo_fecha[1] == "06") $mes = "06";
		if ($arreglo_fecha[1] == "07") $mes = "07";
		if ($arreglo_fecha[1] == "08") $mes = "08";
		if ($arreglo_fecha[1] == "09") $mes = "09";
		if ($arreglo_fecha[1] == "10") $mes = "10";
		if ($arreglo_fecha[1] == "11") $mes = "11";
		if ($arreglo_fecha[1] == "12") $mes = "12";
		
		return $anio.$separador.$mes.$separador.$dia;
	}
	
	function fecha_organizada_mes_completo_hora($fecha, $separador)
	{
		$arreglo_temp  = explode(" ", $fecha);		
		
		$fecha = fecha_organizada_mes_completo($fecha, $separador);
		
		return $fecha." ".$arreglo_temp[1];
	}
	
	function fecha_organizada_mes_truncado_hora($fecha, $separador)
	{
		$arreglo_temp  = explode(" ", $fecha);		
		
		$fecha = fecha_organizada_mes_truncado($fecha, $separador);
		
		return $fecha." ".$arreglo_temp[1];
	}
	
	function truncar ($str, $length=10, $trailing='...') 
	{	
		// take off chars for the trailing
		$length -= mb_strlen($trailing);
		
		if (mb_strlen($str)> $length) 
		{
			return mb_substr($str,0,$length).$trailing;
		} 
		else 
		{ 
			// string was already short enough, return the string
			$res = $str; 
		}
	  
		return $res;
	}
	
	function obtener_catalogo_estados()
	{
		$estados = array(
			'Aguascalientes',
			'Baja California',
			'Baja California Sur',
			'Campeche',
			'Chiapas',
			'Chihuahua',
			'Coahuila',
			'Colima',
			'Distrito Federal',
			'Durango',
			'Estado de México',
			'Guanajuato',
			'Guerrero',
			'Hidalgo',
			'Jalisco',
			'Michoacán',
			'Morelos',
			'Nayarit',
			'Nuevo León',
			'Oaxaca',
			'Puebla',
			'Querétaro',
			'Quintana Roo',
			'San Luis Potosí',
			'Sinaloa',
			'Sonora',
			'Tabasco',
			'Tamaulipas',
			'Tlaxcala',
			'Veracruz',
			'Yucatán',
			'Zacatecas'
		);
		
		return $estados;
	}