		<!DOCTYPE html>
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<title>Login Portal Administrativo</title>
		<link rel="stylesheet" href="<?php echo base_url().'assets/css/foundation/foundation.css'?>">
		<link rel="stylesheet" href="<?php echo base_url().'assets/css/estilos.css'; ?>" media="screen" />
		<link rel="stylesheet" href="<?php echo base_url().'assets/css/mobile/ipad.css'?>">
		<link rel="stylesheet" href="<?php echo base_url().'assets/css/mobile/mq767.css'?>">
		<script src="<?php echo base_url().'assets/js/jquery.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/modernizr.foundation.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/app.js'?>"></script>
		
	</head>
	<body>
		<div id="login">
			<div id="login-content" class="content">
				<div class="row h100">
					<div class="twelve columns table h100">
						<div class="table-cell h100 align-middle align-center">
							<div id="mensaje_servidor" style="display:none; width:280px;"></div>
							<div id="form-container">
							<h1 style="font-size:16px; margin-bottom:2px;">Portal Administrativo</h1>
								<form id="login_form" method="post">
								<input type="text" name="user" id="user" style="width: 90%" class="campo" placeholder="Usuario" />
								<input type="password" name="pass" id="pass" style="width: 90%" class="campo"  placeholder="Contraseña" />
								<input type="submit" id="btnEnviar" value="Iniciar Sesion" class="boton" />
								</form>
							</div>
						</div>	
					</div>
				</div>
			</div>
		</div>
		<script>
			$(document).ready(function(){
				$("#form-container").animate({top:'0'},600);
				$('#user').focus();
				
				$('#mensaje_servidor').click(function(){
					$(this).slideUp();
				});
				
				$('#login_form').submit(function(e){
					e.preventDefault();
					
					var user = $('#user').val();
					var pass = $('#pass').val();
					
					if($.trim(user) == '')
					{
						$('#mensaje_servidor').slideUp();
						$('#mensaje_servidor').removeClass();
						$('#mensaje_servidor').addClass('error');
						$('#mensaje_servidor').html('El campo Usuario es obligatorio.');
						$('#mensaje_servidor').slideDown();
						$('#user').focus();
						
						return false;
					}
					
					if($.trim(pass) == '')
					{
						$('#mensaje_servidor').slideUp();
						$('#mensaje_servidor').removeClass();
						$('#mensaje_servidor').addClass('error');
						$('#mensaje_servidor').html('El campo Password es obligatorio.');
						$('#mensaje_servidor').slideDown();
						$('#pass').focus();
						
						return false;
					}
					
					$.ajax({
						cache    : false,
						data     : $('#login_form').serialize(),
						dataType : "json",
						type     : "POST",
						url      : '<?php echo site_url().'/login/validar'; ?>',
						beforeSend: function(){
							$('#btnEnviar').attr('disabled', 'disabled');
						},
						complete : function(){
							$('#btnEnviar').removeAttr('disabled');
						},
						success  : function(data){
							$('#mensaje_servidor').slideUp();
							
							if(data.tipo == 'error')
							{
								$('#mensaje_servidor').removeClass();
								$('#mensaje_servidor').addClass('error');
								$('#mensaje_servidor').html(data.mensaje);
								$('#mensaje_servidor').slideDown();
							}
							else
							{
								location.href = '<?php echo site_url().'/inicio'; ?>';
							}
						}
					});
					
					return false;
				});
			});
		</script>
	</body>
</html>		