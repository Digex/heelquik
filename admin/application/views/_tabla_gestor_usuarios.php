<?php $i=1; foreach($usuarios as $res): ?>
<tr valign="middle" height="30px">
	<td align="left"><?php echo '<span title="'.$res->nombre.'">'.ellipsize($res->nombre, 21, 1).'</span>'; ?></td>
	<td align="left"><?php echo '<span title="'.$res->email.'">'.ellipsize($res->email, 24, 1).'</span>'; ?></td>
	<td align="left"><?php echo $res->usuario; ?></td>
	<td align="left"><?php echo ($res->tipo=='1'?'Gerente':'Agente de Ventas'); ?></td>
	<td align="left"><?php echo fecha_formato($res->fecha_registro, 4); ?></td>
	<td align="left"><?php
		switch($res->status)
		{
			case '0':
				echo '<span class="statverde">Activo</span>';
				break;
			case '1':
				echo '<span class="statama">Inactivo</span>';
				break;
			case '2':
				echo '<span class="statrojo">Eliminado</span>';
				break;
		}
	?></td>
	<td align="center" valign="middle">
	<?php if($res->status != '2'): ?>
		<!-- <table border="0" cellpadding="0" cellspacing="0">
		<tr style="background-color:transparent !important;">
		<td width="33%" style="border:none;"> -->
		<?php if($res->status == '0'): ?>
		<a href="#div_user_edit" title="Editar Usuario" class="user_edit mr8" name="<?php echo 'user_'.$res->id_usuario; ?>"><!--
		--><img src="<?php echo base_url().'assets/images/edit.png'; ?>" /></a>
		<?php endif; ?>
		<!-- </td>
		<td width="33%" style="border:none;"> -->
		<a class="iconoStatus mr8" href="<?php echo site_url().'/usuarios/'.($res->status == '0'?'desactivar_':'activar_').'usuario/'.$res->id_usuario.'/'.$this->seguridad_modelo->generar_sello($res->id_usuario); ?>" title="<?php echo ($res->status == '0'?'Desactivar Usuario':'Activar Usuario'); ?>">
		<img src="<?php echo base_url().'assets/images/'.($res->status == '0'?'deactivate':'activate').'.png'; ?>" /></a>
		<!-- </td>
		<td width="33%" style="border:none;"> -->
		<a class="iconoBorrar" href="<?php echo site_url().'/usuarios/eliminar/'.$res->id_usuario.'/'.$this->seguridad_modelo->generar_sello($res->id_usuario); ?>" title="Eliminar Usuario"><!--
		!--><img src="<?php echo base_url().'assets/images/delete.png'; ?>" /></a>
		<!-- </td>
		</tr>
		</table> -->
	<?php endif; ?>
	</td>
</tr>
<?php $i++; endforeach;
if($i == 1): ?>
<tr height="40px">
	<td align="center" colspan="9">Sin resultados.</td>
</tr>
<?php endif; ?>