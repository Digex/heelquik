<script>
$(document).ready(function(){
	
	$('#txtBusqueda').focus();

	
	$('#main-container').css({"height" : $('#usuarios-container').height() + 50});


	/********************
	* POPUPS ADD & EDIT
	********************/
	$("#add_button").click(function() {
	      $('#formulario_agregar_usuario')[0].reset();
	      $("#form_agregar").reveal();
	});

	/******************
	* AGREGAR USUARIO
	******************/
	$('#formulario_agregar_usuario').submit(function(e){
		e.preventDefault();


		$.ajax({
			cache    : false,
			data     : $('#formulario_agregar_usuario').serialize(),
			dataType : "json",
			type     : "POST",
			url      : '<?php echo site_url().'/usuarios/agregar'; ?>',
			
			beforeSend: function(){
				$('#btnGuardar').attr('disabled', 'disabled');
				$('#mensaje_servidor').slideUp();
				$('#mensaje_usuario').slideUp();
				$('.error_msj').slideUp();
			},
			complete : function(){
				$('#btnGuardar').removeAttr('disabled');
			},
			success  : function(data){
				if(data.tipo == 'error_campo')
				{
					$('#'+data.campo).html(data.mensaje);
					$('#'+data.campo).slideDown();
				}
				else
				{
					if(data.tipo == 'error')
					{
						$('#mensaje_usuario').removeClass();
						$('#mensaje_usuario').addClass(data.tipo);
						$('#mensaje_usuario').html(data.mensaje);
						$('#mensaje_usuario').slideDown();
					}
					else
					{	
						$('#formulario_agregar_usuario')[0].reset();
						$('#form_agregar').trigger('reveal:close');

						$('#mensaje_servidor').removeClass();
						$('#mensaje_servidor').addClass(data.tipo);
						$('#mensaje_servidor').html(data.mensaje);
						$('#mensaje_servidor').slideDown();
						
						setTimeout(function(){location.reload(); },1000) ;
					}
				}
			}
		});
		
		return false;
	});
	
	/********************
	* ELIMINAR USUARIO
	*********************/
	$('.iconoBorrar').on('click', function(e){
		e.preventDefault();
		
		if(confirm('Se eliminará el usuario. ¿Está seguro?'))
		{
			$.ajax({
				cache    : false,
				dataType : "json",
				type     : "POST",
				url      : $(this).attr('href'),
				success  : function(data){
					$('#mensaje_servidor').slideUp();
					$('#mensaje_servidor').removeClass();
					$('#mensaje_servidor').addClass(data.tipo);
					$('#mensaje_servidor').html(data.mensaje);
					$('#mensaje_servidor').slideDown();
					
					setTimeout(function(){location.reload(); },1000) ;
				}
			});
		}
		
		return false;
	});

	/********************
	* EDITAR USUARIO
	*********************/
	$(".user_edit").on('click', function(){
			 $("#form_editar").reveal();
			 setTimeout(function(){$('.error_msj').slideUp()},1000) ;
			var name = $(this).attr('name');
			var res  = name.split('_');
			var tipo = res[0];// palabra user
			var id   = res[1];
			
			$('#id_usuario').val(id);
			
			$.ajax({
				cache    : false,
				data     : 'id='+id,
				dataType : "json",
				type     : "POST",
				url      : '<?php echo site_url().'/usuarios/edicion'; ?>',
				success  : function(data){
					$('#editNombre').val(data.nombre);
					$('#editEmail').val(data.email);
					$('#editUsuario').val(data.usuario);
					$('#editPerfil').val(data.tipo);
					
					$('#id_original').val(data.usuario);
				}
			});
		});

		$('#formulario_editar_usuario').submit(function(e){
			e.preventDefault();
			

			$.ajax({
				cache    : false,
				data     : $('#formulario_editar_usuario').serialize(),
				dataType : "json",
				type     : "POST",
				url      : '<?php echo site_url().'/usuarios/editar'; ?>',
				beforeSend: function(){
					$('#btnEditar').attr('disabled', 'disabled');
					$('#mensaje_servidor').slideUp();
					$('#mensaje_user_edit').slideUp();
					$('.error_msj').slideUp();
				},
				complete : function(){
					$('#btnEditar').removeAttr('disabled');
				},
				success  : function(data){
					if(data.tipo == 'error_campo')
					{
						$('#'+data.campo).html(data.mensaje);
						$('#'+data.campo).slideDown();
					}
					else
					{
						if(data.tipo == 'error')
						{
							$('#mensaje_user_edit').removeClass();
							$('#mensaje_user_edit').addClass(data.tipo);
							$('#mensaje_user_edit').html(data.mensaje);
							$('#mensaje_user_edit').slideDown();
						}
						else
						{
							$('#formulario_editar_usuario')[0].reset();
							$('#form_editar').trigger('reveal:close');
							
							$('#mensaje_servidor').removeClass();
							$('#mensaje_servidor').addClass(data.tipo);
							$('#mensaje_servidor').html(data.mensaje);
							$('#mensaje_servidor').slideDown();
							
							setTimeout(function(){location.reload(); },1000) ;
						}
					}
				}
			});
			
			return false;
		});
	

	/****************************
	* ACTIVAR DESACTIVAR USUARIO
	****************************/
	

	$('.iconoStatus').click(function(e){
		e.preventDefault();
		exec = confirm('¿Estás seguro de realizar la operación?');
		if(exec == true)
		{
			
			$.ajax({
				cache    : false,
				dataType : "json",
				type     : "POST",
				url      : $(this).attr('href'),
				success  : function(data){
					$('#mensaje_servidor').slideUp();
					$('#mensaje_servidor').removeClass();
					$('#mensaje_servidor').addClass(data.tipo);
					$('#mensaje_servidor').html(data.mensaje);
					$('#mensaje_servidor').slideDown();
					
					setTimeout(function(){location.reload(); },1000) ;
					
				}
			});
		}
		
	});
	/*****************
	* BUSQUEDA 
	*****************/
		$('#form_busqueda').submit(function(e){
		e.preventDefault();
		
		$.ajax({
			cache    : false,
			data     : $('#form_busqueda').serialize(),
			dataType : "json",
			type     : "POST",
			url      : '<?php echo site_url().'/usuarios/busqueda'; ?>',
			beforeSend: function(){
				$('#btnEnviar').attr('disabled', 'disabled');
			},
			complete : function(){
				$('#btnEnviar').removeAttr('disabled');
			},
			success  : function(data){
				$('#mensaje_servidor').slideUp();
				$('#resultados').html(data.html);
				$('#total_reg').html(data.total + ' Registro(s) Mostrados(s)');
			}
		});
		
		return false;
	});

	$('#btnLimpiar').click(function(){
		// limpiar campos alternos y reiniciar los rangos
		$('#txtBusqueda').val('');
		$('#cmbMostrar').val(1);
		$('#cmbOrden').val(1);
		$('#cmbStatus').val(0);
		
		$('#form_busqueda').trigger('submit');
	});

});
</script>

<div id="usuarios-container">
	<div class="row">
		<div class="twelve columns div_formulario">
		<div id="mensaje_servidor" style="display:none; width:925px;"></div>
		<h3>Gestor de Usuarios</h3>
			<div id="form-container" class="form_style">	
				<form id="form_busqueda" method="post">
					<label for="">Buscar:</label>
					<input id="txtBusqueda" type="text" name="busqueda" autocomplete="off" maxlength="200" />
					<label for="cmbStatus">Status:</label>
					<select id="cmbStatus" name="status" class="campo">
						<option value="0">Todos</option>
						<option value="1">Activo</option>
						<option value="2">Inactivo</option>
					</select>
					<label for="cmbOrden">Ordenamiento:</label>
					<select id="cmbOrden" name="orden" class="campo">
								<option value="1">Ascendente</option>
								<option value="2">Descendente</option>
					</select>
					<div id="botones">
						<input type="submit" id="btnEnviar" value="Buscar"  />
						<input type="button" id="btnLimpiar" value="Limpiar" />
					</div>
					
				</form>
			</div>	
		</div>
	</div>

	<div class="row">
		<div class="twelve columns">
			<div id="data-container">
				<a href="#" id="add_button"><img src="<?php echo base_url().'assets/images/add.png'; ?>" /></a>
				<div id="counter-data">
					<span id="total_reg"><?php echo count($usuarios); ?> Registro(s) Mostrados(s)</span>
				</div>
				

				<table border="0" cellspacing="0" cellpadding="0" class="responsive">
					<thead>
						<tr>
							<th>Nombre</th>
							<th >Email</th>
							<th >Usuario</th>
							<th>Tipo</th>
							<th >Fecha Registro</th>
							<th>Status</th>
							<th>Opciones</th>
						</tr>
					</thead>
					<tbody id="resultados">
						<?php $this->load->view('_tabla_gestor_usuarios'); ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>



<!-- form alta -->
<div id="form_agregar" class="reveal-modal medium">
	<div class="row">
		<div class="twelve columns">
			<h3>Agregar Usuario</h3>
			<div id="mensaje_usuario" style="display:none; width:325px;"></div>
			<form id="formulario_agregar_usuario" class="form_style">
				<label for="">Nombre: </label>
				<input id="txtNombre" name="nombre" type="text" maxlength="200" placeholder="Inserte el nombre completo del usuario" autocomplete="off" />
				<div class="error_msj" id="msjNombre" style="display:none;"></div>

				<label for="">Email: </label>
				<input id="txtEmail" name="email" type="text" maxlength="200" placeholder="Inserte el email, ejemplo: ejemplo@ejemplo.com" autocomplete="off" />
				<div class="error_msj" id="msjEmail" style="display:none;"></div>

				<label for="">Usuario: </label>
				<input id="txtUsuario" name="usuario" type="text" maxlength="200" placeholder="Inserte el nombre con el cual accedera al sistema" autocomplete="off" />
				<div class="error_msj" id="msjUsuario" style="display:none;"></div>

				<label for="">Tipo: </label>
				<select name="perfil">
					<option value="2">Cajero</option>
					<option value="1">Administrador</option>
				</select>
				<div class="error_msj" id="msjNombre" style="display:none;"></div>

				<label for="">Password: </label>
				<input id="txtPassword" name="password" type="password" maxlength="200" placeholder="Ingrese la contraseña" autocomplete="off" />
				<div class="error_msj" id="msjPassword" style="display:none;"></div>

				<input id="btnGuardar" class="boton" type="submit" value="Guardar" >
			</form>
		</div>
	</div>
	
</div>

<!-- form edicion -->
<div id="form_editar" class="reveal-modal medium">
	<div class="row">
		<div class="twelve columns">
			<div id="mensaje_user_edit" style="display:none; width:325px;"></div>
			<h3>Agregar Usuario</h3>
			<div id="mensaje_usuario" style="display:none; width:325px;"></div>
			<form id="formulario_editar_usuario" class="form_style">
				<label for="">Nombre: </label>
				<input id="editNombre" name="nombre" type="text" maxlength="200" placeholder="Inserte el nombre completo del usuario" autocomplete="off" />
				<div class="error_msj" id="msjNombreEdit" style="display:none;"></div>

				<label for="">Email: </label>
				<input id="editEmail" name="email" type="text" maxlength="200" placeholder="Inserte el email, ejemplo: ejemplo@ejemplo.com" autocomplete="off" />
				<div class="error_msj" id="msjEmailEdit" style="display:none;"></div>

				<label for="">Usuario: </label>
				<input id="editUsuario" name="usuario" type="text" maxlength="200" placeholder="Inserte el nombre con el cual accedera al sistema" autocomplete="off" />
				<div class="error_msj" id="msjUsuarioEdit" style="display:none;"></div>

				<label for="">Tipo: </label>
				<select name="perfil"  id="editPerfil">
					<option value="2">Cajero</option>
					<option value="1">Administrador</option>
				</select>
				<div class="error_msj" id="msjNPerfilEdit" style="display:none;"></div>

				<label for="">Password: </label>
				<input id="editPassword" name="password" type="password" maxlength="200" placeholder="Ingrese la contraseña" autocomplete="off" />
				<div class="error_msj" id="msjPasswordEdit" style="display:none;"></div>
				
				<input id="id_usuario" name="id" type="hidden" />
				<input id="id_original" name="original" type="hidden" />
				<input id="btnEditar" class="boton" type="submit" value="Editar" >
			</form>
		</div>
	</div>	
</div>