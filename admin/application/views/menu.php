<?php $auth = $this->session->userdata('auth'); ?>
		<div id="menu">
			  <nav class="top-bar show-for-small">
			    <ul class="title-area">
			      <!-- Title Area -->
			      <li class="name">
			      </li>
			      <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
			      <li class="toggle-topbar menu-icon">
			        <a href="#">
			          <!-- <span>Menu</span>
			        -->
			      </a>
			    </li>
			  </ul>
			  <section class="top-bar-section">
			    <!-- Left Nav Section -->
			    <ul class="left">
			      <li class="divider"></li>
			      <li>
			        <a href="<?php echo site_url()?>/ordenes">Ordenes</a>
			      </li>
			      <li>
			         <a href="<?php echo site_url()?>/servicios">Servicios</a>
			      </li>
			      <li>
			        <a href="<?php echo site_url()?>/departamentos">Departamentos</a>
			      </li>
			      <li>
			        <a href="<?php echo site_url()?>/sucursales">Sucursales</a>
			      </li>
			      <li>
			         <a href="<?php echo site_url()?>/maquiladores">Maquiladores</a>
			      </li>
			      <li>
			      	<a href="<?php echo site_url()?>/cortes">Cortes</a>
			      </li>
			      <li>
			      	<a href="<?php echo site_url()?>/arqueo">Arqueo</a>
			      </li>
			      <li>
			      	<a href="<?php echo site_url()?>/usuarios">Usuarios</a>
			      </li>
			    </ul>
			    <!-- Right Nav Section --> 
			    </section>
			</nav>

			<div class="row hide-for-small">
			  <div class="text-align-center" id="nav">
			    <div id="main-nav">
			      
			      <a href="<?php echo site_url()?>/ordenes">Ordenes</a>
			      <a href="<?php echo site_url()?>/servicios">Servicios</a>
			      <a href="<?php echo site_url()?>/departamentos">Departamentos</a>
			      <a href="<?php echo site_url()?>/sucursales">Sucursales</a>
			      <a href="<?php echo site_url()?>/maquiladores">Maquiladores</a>
			      <a href="<?php echo site_url()?>/cortes">Cortes</a>
			      <a href="<?php echo site_url()?>/arqueo">Arqueo</a>
			      <a href="<?php echo site_url()?>/usuarios">Usuarios</a>
			    </div>
			  </div>
			</div>
		</div>

		<script type="text/javascript">

			$(document).ready(function(){
				$("#menu #main-nav a").each(function(){
					var selector = $(this);
					testAnim("lightSpeedIn", selector);
				});

				$("#menu #main-nav a").mouseover(function(){
					var selector = $(this);
					testAnim("pulse", selector);
				});

			});

			function testAnim(x, selector) {
			  selector.removeClass().addClass(x + ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
			    $(this).removeClass();
			  });
			};

		</script>