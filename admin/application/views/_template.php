<!DOCTYPE html>
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<title>Login Portal Administrativo</title>
		<link rel="stylesheet" href="<?php echo base_url().'assets/css/foundation/foundation.css'?>">
		<link rel="stylesheet" href="<?php echo base_url().'assets/css/responsive-tables.css'?>">
		<link rel="stylesheet" href="<?php echo base_url().'assets/css/animate.css'?>">
		<link rel="stylesheet" href="<?php echo base_url().'assets/css/estilos.css'; ?>" media="screen" />
		<link rel="stylesheet" href="<?php echo base_url().'assets/css/mobile/ipad.css'?>">
		<link rel="stylesheet" href="<?php echo base_url().'assets/css/mobile/mq767.css'?>">

		<script src="<?php echo base_url().'assets/js/jquery.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/jquery-ui-1.10.3.custom.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/modernizr.foundation.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/foundation.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/responsive-tables.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/jquery.foundation.topbar.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/app.js'?>"></script>
		
	</head>
	<body>
		<?php $is_home = $this->router->fetch_class() === 'login' ? true : false;?>
		<?php if(!$is_home):?>
		<div id="header">
			<div class="row">
				<div class="twelve columns">
					<h2>Heel Quik</h2>
					<span> Bienvenido: <i><?php echo $usuario['nombre']?></i></span>
					<a href="<?php echo site_url()?>/login/logout">Salir</a>
				</div>
			</div>
		</div>
		<?php endif;?>
		<?php $this->load->view('menu'); ?>
		<div id="main-container">
			
			<?php $this->load->view($pagina); ?>
		</div>
		<?php $is_home = $this->router->fetch_class() === 'login' ? true : false;?>
		<?php if(!$is_home):?>
			<div id="footer">
				<div class="row">
					<div class="twelve columns">
						
					</div>
				</div>
			</div>
		<?php endif;?>
	</body>
</html>