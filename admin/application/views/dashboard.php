dashboard<!-- <div id="main-form">
	<div id="mensaje_servidor" class="" style="display:none"></div>
	<form id="form_datos_principales" method="post">
	<div class="row">
		<div id="cedula_mensaje" class="error" style="display:none"></div>
		<div class="six columns">
			<label for="">Cedula:</label>
				<input type="text" name="cedula" id="cedula" autocomplete="on" placeholder="xxx-xxxxxxx-x" />
		</div>
	</div>
	<div class="row">
		<div id="nombre_mensaje" class="error" style="display:none"></div>
		<div class="six columns">
			<label for="">Nombre :</label>
				<input type="text" name="nombre" id="nombre" autocomplete="on"  placeholder="Inserte su nombre" />
		</div>
	</div>
	<div class="row">
		<div id="a_paterno_mensaje" class="error" style="display:none"></div>
		<div class="six columns">
			<label for="">Primer apellido:</label>
				<input type="text" name="a_paterno" id="a_paterno" autocomplete="on"  placeholder="Inserte su primer apellido" />
		</div>
	</div>
	<div class="row">
		<div id="a_materno_mensaje" class="error" style="display:none"></div>
		<div class="six columns">
			<label for="">Segundo apellido:</label>
				<input type="text" name="a_materno" id="a_materno" autocomplete="on"  placeholder="Inserte su segundo apellido" />
		</div>
	</div>
	<div class="row">
		<div id="fecha_nacimiento_mensaje" class="error" style="display:none"></div>
		<div class="six columns">
			<label for="">Fecha de nacimiento:</label>
				<input type="text" name="fecha_nacimiento" id="fecha_nacimiento" autocomplete="on" readonly="readonly"  />
		</div>
	</div>
	
	<div class="row">
		<div id="sexo_mensaje" class="error" style="display:none"></div>
		<div class="six columns" id="sexo">
			<label for="">Sexo:</label>
			<div id="opciones-sexo">
				<input type="radio" name="sexo" value="Masculino">Masculino<br>
				<input type="radio" name="sexo" value="Femenino">Femenino
			</div>
		</div>
	</div>
	<div class="row">
		<div id="direccion_mensaje" class="error" style="display:none"></div>
		<div class="six columns">
			<label for="">Dirección:</label>
				<input type="text" name="direccion" id="direccion" autocomplete="on"  placeholder="Inserte su direccion" />
		</div>
	</div>
	<div class="row">
		<div id="telefono_mensaje" class="error" style="display:none"></div>
		<div class="six columns">
			<label for="">Teléfono :</label>
				<input type="text" name="telefono" id="telefono" autocomplete="on"  placeholder="Inserte su numero telefonico" />
		</div>
	</div>
	<div class="row">
		<div id="provincia_mensaje" class="error" style="display:none"></div>
		<div class="six columns">
			<label for="">Provincia :</label>
				<select name="provincia" id="provincia" >
				<option value="0" selected="selected">Seleccione una provincia</option>
				<?php foreach ($provincias as $provincia): ?>
					<option value="<?php echo $provincia->provincia?>"> <?php echo $provincia->provincia?></option>
				<?php endforeach; ?>
				</select>
		</div>
	</div>
	<div class="row">
		<div id="municipio_mensaje" class="error" style="display:none"></div>
		<div class="six columns">
			<label for="">Municipio :</label>
			<select name="municipios" id="municipios">
			</select>
		</div>
	</div>
	<div class="row">
		<div class="six columns " id="button-submit">
			<input type="submit" id="btnEnviar" value="Enviar" class="boton" />
		</div>
	</div>
		
	</form>
</div>

<script type="text/javascript">
	
	$(document).ready(function(){
		
		$('#cedula').focus();	
		
		$('#form_datos_principales').submit(function(){
			
			var validar = true;
			
			if( !validar_cedula('#cedula', '#cedula_mensaje') ) validar = false;
			if( !validar_texto('#nombre', '#nombre_mensaje', '3', '250') ) validar = false;
			if( !validar_texto('#a_paterno', '#a_paterno_mensaje', '3', '250') ) validar = false;
			if( !validar_texto('#a_materno', '#a_materno_mensaje', '3', '250') ) validar = false;
			if( !validar_texto('#fecha_nacimiento', '#fecha_nacimiento_mensaje', '3', '250') ) validar = false;
			if( !validar_radio('sexo', '#sexo_mensaje') ) validar = false;
			if( !validar_texto('#direccion', '#direccion_mensaje', '3', '250') ) validar = false;
			if( !validar_numero('#telefono', '#telefono_mensaje', '7', '20') ) validar = false;
			if( !validar_seleccion('#provincia', '#provincia_mensaje') ) validar = false;
			if( !validar_seleccion('#municipios', '#municipio_mensaje') ) validar = false;
					
			if(validar)
			{
				if(confirm("Esta seguro de querer enviar estos datos?"))
				{
					$.ajax({
						url: "<?php echo site_url()."/personas/agregar_persona";?>",
						type: "POST",
						cache: false, 
						data: $('#form_datos_principales').serialize(), 
						dataType: 'json',
						success: formulario_respuesta
					});
				}
				else
					return false;

			}
			
			return false;
		});

		$('#provincia').change(function()
		{
			
			$('#municipios').attr('disabled','');
			
			var valor = $("#provincia option:selected").val();
		        	
			if(valor != "")
			{
				$.ajax({
						url: "<?php echo site_url()."/personas/obtener_municipios_single";?>",
						type: "POST",
						cache: false, 
						data: 'provincia='+valor,
						dataType: 'json',
						success: cargar_municipios
					});
			}
			else
			{
				$("#municipios option:contains(Seleccione un municipio)").attr("selected", true);
				$('#municipios').attr('disabled','disabled');
			}
			
		});
	});
	
	function formulario_respuesta(data)
	{		 
		$('#mensaje_servidor').slideUp();

		if(data.tipo_mensaje == "exito")
		{
			$('#mensaje_servidor').removeClass();
			$('#mensaje_servidor').addClass('exito');
			$('#mensaje_servidor').addClass('hide');
			$('#mensaje_servidor').html(data.mensaje);
			$('#mensaje_servidor').slideDown();
			

			$("#form_datos_principales")[0].reset();
			$("#cedula_mensaje").focus();
		}
		else 
		{
			$('#mensaje_servidor').removeClass();
			$('#mensaje_servidor').addClass('error');
			$('#mensaje_servidor').addClass('hide');
			$('#mensaje_servidor').html(data.mensaje);
			$('#mensaje_servidor').slideDown();
			
		}	
	}

		 function cargar_municipios(data)
		 {
			 if(data.municipios.length > 0)
			 {
				 var html = '';
				 html += '<option value="0"> Seleccione un municipio</option>';

				 
				 for(var i in data.municipios)
				 {
					html += '<option value="'+data.municipios[i]["municipio"]+'">'+data.municipios[i]["municipio"]+'</option>';
				 }

				$('#municipios').empty();
				$('#municipios').removeAttr('disabled');
			 	$("#municipios").html(html);
			 
			 } 
	     }
</script>	 -->