<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('usuario_modelo');
	}
	
	public function index()
	{
		$this->load->view('login');
	}
	
	public function validar()
	{
		if(!$this->input->post())
		{
			redirect('login');
			exit();
		}
		
		$user = $this->input->post('user');
		$pass = $this->input->post('pass');
		
		$resultado = $this->usuario_modelo->validar_usuario($user, $pass);
		
		if(empty($resultado))
		{
			$data['tipo']    = 'error';
			$data['mensaje'] = 'El usuario y/o contraseña no son válidos';
		}
		else
		{
			$data['tipo'] = 'exito';
			
			$auth['id_usuario'] = $resultado->id_usuario;
			$auth['nombre']     = $resultado->nombre;
			$auth['email']      = $resultado->email;
			$auth['tipo']       = $resultado->tipo;
			$auth['sello']      = $this->seguridad_modelo->sellar_sesion($auth);
			
			$this->session->set_userdata('auth', $auth);
		}
		
		echo json_encode($data);
		exit();
	}
	
	public function logout()
	{
		$this->session->unset_userdata("auth");
		redirect('login', 'refresh');
		exit();
	}
}