<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('usuario_modelo');
		//$this->load->model('perfiles_modelo');
		
		$auth = $this->session->userdata('auth');
		date_default_timezone_set('America/Mexico_City');
		
		if($auth['sello'] != $this->seguridad_modelo->sellar_sesion($auth))
		{
			redirect("login/logout");
			exit();
		}
		
		if($auth['tipo'] != '1')
		{
			redirect('inicio');
			exit();
		}
	}
	
	public function index()
	{
		$data['usuario']    = $this->session->userdata('auth');
		//$data['navegacion'] = '<strong>'.anchor('inicio', 'Inicio').'</strong> &raquo; Gestor de Usuarios';
		$data['pagina']     = 'gestor_usuarios';
		$data['usuarios']   = $this->usuario_modelo->obtener_usuarios();
		//$data['perfiles']   = $this->perfiles_modelo->obtener_perfiles();
		
		$this->load->view('_template', $data);
	}
	
	/* Filtro de búsqueda */
	public function busqueda()
	{
		if(!$this->input->post())
		{
			redirect('usuarios');
			exit();
		}
		
		$busqueda = $this->input->post('busqueda');
		$orden    = $this->input->post('orden');
		$status   = $this->input->post('status');
		$busqueda = trim($busqueda);
		$query    = mysql_real_escape_string($busqueda);
		
		$where = "WHERE 1=1 ";
		
		if($query != '')
		{
			$where .= "AND (u.nombre LIKE '%".$query."%' OR u.email LIKE '%".$query."%' OR u.usuario LIKE '%".$query."%') ";
		}
		
		switch($status)
		{
			case '1':
				$where .= "AND u.status = 0 ";
				break;
			case '2':
				$where .= "AND u.status = 1 ";
				break;
			case '3':
				$where .= "AND u.status = 2 ";
				break;
		}
		
		if($orden == '1')// asc
		{
			$where .= "ORDER BY u.id_usuario ASC";
		}
		else// desc
		{
			$where .= "ORDER BY u.id_usuario DESC";
		}
		
		$usuarios = $this->usuario_modelo->obtener_usuarios($where);
		
		$html  = $this->load->view('_tabla_gestor_usuarios', array('usuarios' => $usuarios), true);
		$total = count($usuarios);
		
		echo json_encode(array('html' => $html, 'total' => $total));
	}
	
	/* Agregar Usuario */
	public function agregar()
	{
		if(!$this->input->post())
		{
			redirect('usuario');
			exit();
		}
		
		$nombre   = $this->input->post('nombre');
		$email    = $this->input->post('email');
		$usuario  = $this->input->post('usuario');
		$perfil   = $this->input->post('perfil');
		$password = $this->input->post('password');

		$nombre   = trim($nombre);
		$email    = trim($email);
		$usuario  = trim($usuario);
		$password = trim($password);
		
		if($nombre == '')
		{
			$salida['tipo']    = 'error_campo';
			$salida['campo']   = 'msjNombre';
			$salida['mensaje'] = 'El nombre es obligatorio.';
			
			echo json_encode($salida);
			exit();
		}
		
		if($email == '')
		{
			$salida['tipo']    = 'error_campo';
			$salida['campo']   = 'msjEmail';
			$salida['mensaje'] = 'El email es obligatorio.';
			
			echo json_encode($salida);
			exit();
		}
		
		if(!preg_match('/[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}/', $email))
		{
			$salida['tipo']    = 'error_campo';
			$salida['campo']   = 'msjEmail';
			$salida['mensaje'] = 'El email no tiene formato válido.';
			
			echo json_encode($salida);
			exit();
		}
		
		if($usuario == '')
		{
			$salida['tipo']    = 'error_campo';
			$salida['campo']   = 'msjUsuario';
			$salida['mensaje'] = 'El usuario es obligatorio.';
			
			echo json_encode($salida);
			exit();
		}
		
		if(!preg_match('/^[a-zA-Z0-9_-]{1,15}$/', $usuario))
		{
			$salida['tipo']    = 'error_campo';
			$salida['campo']   = 'msjUsuario';
			$salida['mensaje'] = 'El nombre de usuario no es válido.';
			
			echo json_encode($salida);
			exit();
		}
		
		if($this->usuario_modelo->existe_usuario($usuario))
		{
			$salida['tipo']    = 'error_campo';
			$salida['campo']   = 'msjUsuario';
			$salida['mensaje'] = 'El nombre de usuario ya está registrado.';
			
			echo json_encode($salida);
			exit();
		}
		
		if($password == '')
		{
			$salida['tipo']    = 'error_campo';
			$salida['campo']   = 'msjPassword';
			$salida['mensaje'] = 'El password es obligatorio.';
			
			echo json_encode($salida);
			exit();
		}
		
		$datos['tipo']           	 = $perfil;
		$datos['nombre']         	 = $nombre;
		$datos['email']          	 = strtolower($email);
		$datos['usuario']        	 = $usuario;
		$datos['password']       	 = md5($password);
		$datos['fecha_registro'] 	 = date('Y-m-d H:i:s');
		$datos['fecha_modificacion'] = date('Y-m-d H:i:s');
		
		if(!$this->usuario_modelo->insertar_usuario($datos))
		{
			$salida['tipo']    = 'error';
			$salida['mensaje'] = 'Ocurrió un error al guardar el registro.';
			
			echo json_encode($salida);
			exit();
		}
		
		$salida['tipo']    = 'exito';
		$salida['mensaje'] = 'Se agregó correctamente el usuario.';
		
		echo json_encode($salida);
		exit();
	}
	
	/* Editar Usuario */
	public function editar()
	{
		if(!$this->input->post())
		{
			redirect('usuario');
			exit();
		}
		
		$clave    = $this->input->post('id');
		$nombre   = $this->input->post('nombre');
		$email    = $this->input->post('email');
		$perfil   = $this->input->post('perfil');
		$nombre   = trim($nombre);
		$email    = trim($email);
		
		if($nombre == '')
		{
			$salida['tipo']    = 'error_campo';
			$salida['campo']   = 'msjNombreEdit';
			$salida['mensaje'] = 'El nombre es obligatorio.';
			
			echo json_encode($salida);
			exit();
		}
		
		if($email == '')
		{
			$salida['tipo']    = 'error_campo';
			$salida['campo']   = 'msjEmailEdit';
			$salida['mensaje'] = 'El email es obligatorio.';
			
			echo json_encode($salida);
			exit();
		}
		
		if(!preg_match('/[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}/', $email))
		{
			$salida['tipo']    = 'error_campo';
			$salida['campo']   = 'msjEmailEdit';
			$salida['mensaje'] = 'El email no tiene formato válido.';
			
			echo json_encode($salida);
			exit();
		}
		
		if($this->input->post('usuario'))
		{
			$usuario  = $this->input->post('usuario');
			$password = $this->input->post('password');
			$usuario  = trim($usuario);
			$password = trim($password);
			$original = $this->input->post('original');
			
			if($usuario == '')
			{
				$salida['tipo']    = 'error_campo';
				$salida['campo']   = 'msjUsuarioEdit';
				$salida['mensaje'] = 'El usuario es obligatorio.';
				
				echo json_encode($salida);
				exit();
			}
			
			if(!preg_match('/^[a-zA-Z0-9_-]{1,15}$/', $usuario))
			{
				$salida['tipo']    = 'error_campo';
				$salida['campo']   = 'msjUsuarioEdit';
				$salida['mensaje'] = 'El nombre de usuario no es válido.';
				
				echo json_encode($salida);
				exit();
			}
			
			if($usuario != $original)
			{
				if($this->usuario_modelo->existe_usuario($usuario))
				{
					$salida['tipo']    = 'error_campo';
					$salida['campo']   = 'msjUsuarioEdit';
					$salida['mensaje'] = 'El nombre de usuario ya está registrado.';
					
					echo json_encode($salida);
					exit();
				}
			}
			
			if($password == '')
			{
				$salida['tipo']    = 'error_campo';
				$salida['campo']   = 'msjPasswordEdit';
				$salida['mensaje'] = 'El password es obligatorio.';
				
				echo json_encode($salida);
				exit();
			}
			
			$datos['usuario']  = $usuario;
			$datos['password'] = md5($password);
		}
		
		$datos['tipo'] 		= $perfil;
		$datos['nombre']    = $nombre;
		$datos['email']     = $email;
		
		$this->usuario_modelo->actualizar_usuario($clave, $datos);
		
		$salida['tipo']    = 'exito';
		$salida['mensaje'] = 'Se actualizó correctamente el usuario.';
		
		echo json_encode($salida);
		exit();
	}
	
	public function edicion()
	{
		if(!$this->input->post())
		{
			redirect('usuarios');
			exit();
		}
		
		$id = $this->input->post('id');
		
		$datos = $this->usuario_modelo->obtener_usuario_detalle($id);
		
		$salida['nombre']    = $datos->nombre;
		$salida['email']     = $datos->email;
		$salida['usuario']   = $datos->usuario;
		$salida['tipo'] = $datos->tipo;
		
		echo json_encode($salida);
		exit();
	}
	
	// activaciones y desactivaciones
	public function desactivar_usuario($id = '', $sello = '')
	{
		if(!$this->seguridad_modelo->verificar_sello($id, $sello))
		{
			redirect('usuario');
			exit();
		}
		
		$datos['status'] = 1; // 1 es desactivado
		
		if($this->usuario_modelo->actualizar_usuario($id, $datos) > 0)
		{
			$data['tipo']    = 'exito';
			$data['mensaje'] = 'Se ha desactivado correctamente el usuario.';
		}
		else
		{
			$data['tipo']    = 'error';
			$data['mensaje'] = 'Ocurrió un error al actualizar el usuario.';
		}
		
		echo json_encode($data);
		die();
	}
	
	public function activar_usuario($id = '', $sello = '')
	{
		if(!$this->seguridad_modelo->verificar_sello($id, $sello))
		{
			redirect('usuario');
			exit();
		}
		
		$datos['status'] = 0; // 1 es desactivado
		
		if($this->usuario_modelo->actualizar_usuario($id, $datos) > 0)
		{
			$data['tipo']    = 'exito';
			$data['mensaje'] = 'Se ha activado correctamente el usuario.';
		}
		else
		{
			$data['tipo']    = 'error';
			$data['mensaje'] = 'Ocurrió un error al actualizar el usuario.';
		}
		
		echo json_encode($data);
		die();
	}
	
	public function eliminar($id = '', $sello = '')
	{
		if(!$this->seguridad_modelo->verificar_sello($id, $sello))
		{
			redirect('usuario');
			exit();
		}
		
		
		if($this->usuario_modelo->eliminar_usuario($id)> 0)
		{
			$data['tipo']    = 'exito';
			$data['mensaje'] = 'Se ha eliminado correctamente el usuario.';
		}
		else
		{
			$data['tipo']    = 'error';
			$data['mensaje'] = 'Ocurrió un error al actualizar el usuario.';
		}
		
		echo json_encode($data);
	}
}