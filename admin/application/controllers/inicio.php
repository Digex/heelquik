<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inicio extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		//$this->load->model('cliente_modelo');
		
		$auth = $this->session->userdata('auth');
		
		if($auth['sello'] != $this->seguridad_modelo->sellar_sesion($auth))
		{
			redirect("login/logout");
			exit();
		}
	}
	
	public function index()
	{
		$data['usuario']    = $this->session->userdata('auth');
		$data['pagina']     = 'dashboard';
		//$data['licencias']  = $this->cliente_modelo->dashboard_licencias();
		//$data['timbres']    = $this->cliente_modelo->dashboard_timbres();
		//$data['soporte']    = $this->cliente_modelo->dashboard_soporte();
		
		$this->load->view('_template', $data);
	}
}